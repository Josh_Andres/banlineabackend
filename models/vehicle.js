'use strict';
const mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
require('../db/db');

const vehicleSchema = new mongoose.Schema({
	_id: {type: Number},
	vehicleType: {type: String},
	plates: {type: String},
	dateSOAT: {type: Date},
	shipping: {type: String},
	driver: {type: String},
	status: {type: String},
	coordinates :{type: String}
},  { _id: false });

vehicleSchema.plugin(AutoIncrement, {id: 'vehicle_id'});
const vehicleModel = mongoose.model('Vehicle', vehicleSchema, 'Vehicle');

module.exports = vehicleModel;
