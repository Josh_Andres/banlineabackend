'use strict';
const mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
require('../db/db');

const driverSchema = new mongoose.Schema({
	_id: {type: Number},
	name: {type: String},
	apells: {type: String},
	doctype: {type: String},
	docnumber: {type: String},
	status: {type: String},
	vehicle: {type: Number}
}, { _id: false });

driverSchema.plugin(AutoIncrement, {id: 'driver_id'});
const driverModel = mongoose.model('Driver', driverSchema, 'Driver');

module.exports = driverModel;
