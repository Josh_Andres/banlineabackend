'use strict';
const mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
require('../db/db');

const shippingSchema = new mongoose.Schema({
	_id: {type: Number},
	name: {type: String},
	description: {type: String}
},  { _id: false });

shippingSchema.plugin(AutoIncrement, {id: 'shipping_id'});
const shippingModel = mongoose.model('Shipping', shippingSchema, 'Shipping');

module.exports = shippingModel;
