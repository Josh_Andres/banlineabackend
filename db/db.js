'use strict';
const mongoose = require('mongoose');
const config = require('../config/config');
// URL DB develop
// const url = 'mongodb://'+config.HOST+':'+config.PORTDB+'/'+config.DB;

mongoose.Promise = require('bluebird');
mongoose.set('debug', config.DEBUG);

const connection = mongoose.connect(config.production, {useNewUrlParser: true}, (err, res) =>{
  if (!err) {
    console.log('Connected to database');
  } else {
    console.log('Unable to database conexión');
  }
});

module.exports = connection;
