'use strict';
const createError = require('http-errors');
const express = require('express');
const logger = require('morgan');

const driversRoutes = require('./../routes/drivers');
const vehiclesRouter = require('./../routes/vehicles');
const shippingsRouter = require('./../routes/shippings');

const app = express();

app.use((req, res, next)=>{
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Request-Whit, Content-Type, Accept');
	res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
	next();
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Routes
app.use('/drivers/', driversRoutes);
app.use('/vehicles/', vehiclesRouter);
app.use('/shipping/', shippingsRouter);

// catch 404
app.use(function(req, res, next) {
  next(createError(404));
});

// handler
app.use((err, req, res, next) => {
  // show the error page
  res.status(err.status || 500);
  res.send({message: 'not found'});
});

module.exports = app;
