'use strict';
const _ = require('lodash');
const Driver = require('../../models/driver');

/* ----POST create a new driver---- */
/* validate data */
exports.validData = (req, res, next) => {
	let driver = ['name', 'apells', 'doctype', 'docnumber'];
	let validate = true;
	_.forEach(driver, (value)=> {
			if (!req.body[value]){
				validate = false;
			}
	});
	validate ? next() :  res.status(400).send({message: 'Campos Incompletos', error: 'Fields not found'});
};

/* save driver data */
exports.saveDriver = (req, res) => {
	let driver = new Driver(req.body);

	driver.save((err, driver) => {
		if (err) {
			return res.status(400).send({message: 'Conductor no creado', driver});
		} else {
			return res.status(200).send({message: 'Conductor creado'});
		}
	})
};

/* ----GET return a single driver---- */
/* return driver data */
exports.getDriver = (req, res) => {
	Driver.findOne({_id: req.params.driver}).then((driver) => {
		return res.status(200).send({message: 'Conductor econtrado', driver});
	}).catch((err) => {
		return res.status(400).send({message: 'Error al consultar los datos de conductor', err: err});
	});
};

/* return drivers data */
exports.getDrivers = (req, res) => {
	let query = {};
	if (req.query.list) {
		query = {status:{$ne: 'assigned'}};
	}
	Driver.find(query).then((drivers) => {
		return res.status(200).send({message: 'Conductores encontrados', drivers});
	}).catch((err) => {
		return res.status(400).send({message: 'Error al consultar conductores', err: err});
	});
};

/* ----PUT update a single driver---- */
/* update driver data */
exports.putDriver = (req, res, next) => {
	Driver.update({_id: req.params.driver}, {$set: req.body}).then((driver) => {
		return res.status(200).send({message: 'Datos de conductor actualizados.', driver});
	}).catch((err)=> {
		return res.status(500).send({message: 'No fue posible actualizar los datos de conductor.', err: err});
	});
};

/* ----DELETE delete a single driver---- */
/* delete driver data */
exports.deleteDriver = (req, res) => {

};