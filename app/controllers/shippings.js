'use strict';
const _ = require('lodash');
const Shipping = require('../../models/shipping');

/* ----POST create a new shipping---- */
/* validate data */
exports.validData = (req, res, next) => {
	let driver = ['name', 'description'];
	let validate = true;
	_.forEach(driver, (value)=> {
			if (!req.body[value]){
				validate = false;
			}
	});
	validate ? next() :  res.status(400).send({message: 'Campos Incompletos', error: 'Not found fields'});
};

/* save shipping data */
exports.saveShipping = (req, res) => {
	let shipping = new Shipping(req.body);

	shipping.save((err, shipping) => {
		if (err) {
			return res.status(400).send({message: 'Flota not created', shipping});
		} else {
			return res.status(200).send({message: 'Flota created'});
		}
	})
};

/* ----GET return a single shipping---- */
/* return driver data */
exports.getShipping = (req, res) => {
	Shipping.find({_id: req.params.shipping}).then((shipping) => {
		return res.status(200).send({message: 'Flota encontrada', shipping});
	}).catch((err) => {
		return res.status(400).send({message: 'Flota no encontrada', err: err});
	});
};

/* return driver data */
exports.getAllShipping = (req, res, next) => {
	Shipping.find({}).then((shipping) => {
		return res.status(200).send({message: 'Flotas encontradas', shipping});
	}).catch((err) => {
		return res.status(400).send({message: 'Flotas no encontradas', err: err});
	});
};

/* ----PUT update a single shipping---- */
/* update driver data */
exports.putShipping = (req, res, next) => {

};

/* ----DELETE delete a single shipping---- */
/* delete driver data */
exports.deleteShipping = (req, res) => {

};