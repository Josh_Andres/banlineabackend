'use strict';
const _ = require('lodash');
const Vehicle = require('../../models/vehicle');
const Driver = require('../../models/driver');

/* POST create a new driver */
/* create driver data */
exports.validData = (req, res, next) => {
	console.log(req.body);
	let vehicle = ['vehicleType', 'plates', 'dateSOAT', 'shipping', 'status', 'coordinates'];
	let validate = true;
	_.forEach(vehicle, (value)=> {
			if (!req.body[value]){
				validate = false;
			}
	});
	validate ? next() :  res.status(400).send({message: 'Campos Incompletos', error: 'Fields not found'});
};

/* save vehicle data */
exports.saveVehicle = (req, res) => {
	let vehicle = new Vehicle(req.body);

	vehicle.save((err, vehicle) => {
		if (err) {
			return res.status(400).send({message: 'No fue posible guardar el vehiculo', err});
		} else {
			return res.status(200).send({message: 'Vehiculo creado', vehicle});
		}
	})
};

/* ----GET return a single vehicle---- */
/* return vehicle data */
exports.getVehicle = (req, res, next) => {
	console.log(req.params.vehicle);
	Vehicle.findOne({_id: req.params.vehicle}).then((vehicle)=> {
		return res.status(200).send({message: 'Vehicle no encontrado', vehicle});
	}).catch((err)=> {
		return res.status(400).send({message: 'Error al consultar vehiculo', err});
	});
};

/* return vehicles data */
exports.getVehicles = (req, res, next) => {
	Vehicle.find({}).then((vehicles)=> {
		return res.status(200).send({message: 'Vehicles encotrandos', vehicles});
	}).catch((err)=> {
		return res.status(400).send({message: 'Error al consultar lista de vehiculos', err: err});
	});
};

/* ----PUT update a single vehicle---- */
/* update vehicle data */
exports.putVehicle = (req, res, next) => {
	Vehicle.update({_id: req.params.vehicle}, {$set: req.body}).then((vehicleupdate) => {
		console.log(vehicleupdate);
		return res.status(200).send({message: 'Vehiculo actualizado', vehicleupdate})
	}, (error) => {
		return res.status(400).send({message: 'Error al intentar actualizar.', error});
	});

};

/* update vehicle data */

exports.validAssigned = (req, res, next) => {
	let valid = ['driver'];
	let flag = true;
	_.forEach((e) => {
		if (!req.body[e]) {
			flag = false;
		}
	})
	flag ? next() : res.status(400).send({message: 'Bad Request', error: 'Fields not found'});
};
exports.validUserAssigned = (req, res, next) => {
	Driver.find({_id: req.body.driver, status:{$ne:'assigned'}}).then((response) => {	
		next()
	}).catch((err)=> {
		return res.send(400).send({message: 'Error al consultar su información', err})
	})
};

exports.putAssigned = (req, res, next) => {
	Vehicle.update({_id: req.params.vehicle}, {$set:{driver: req.body.driver, status: 'assigned'}})
	.then((assigned) => {
		return Driver.update({_id: req.body.driver}, {$set: {vehicle: parseInt(req.params.vehicle), status: 'assigned'}})
	}).then((vehicle)=> {
			return res.status(200).send({message: 'Vehiculo asignado', assigned: vehicle});
	}).catch((error) => {
		return res.status(500).send({message: 'Error al intentar actualizar', error})
	});
};

/* ----DELETE delete a single vehicle---- */
/* delete vehicle data */
exports.deleteVehicle = (req, res) => {

};