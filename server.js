'use strict';

const app = require('./app/app');
const debug = require('debug')('banlinea:server');
const http = require('http');
require('dotenv').config({path: __dirname + '/.env'});


const port = process.env.PORT || '3000';
app.set('port', port);

/**
 * Create HTTP server.
 */

const server = http.createServer(app);

/**
 * Listen on provided porp
 */

server.listen(port);
server.on('listening', onListening);


/**
 * Event listener
 */
function onListening() {
  const addr = server.address();
  let bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}
