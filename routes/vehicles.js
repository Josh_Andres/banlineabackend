'use strict';
const express = require('express');
const router = express.Router();
const {
  getVehicle,
  getVehicles,
  saveVehicle,
  validData,
  putVehicle,
  deleteVehicle,
  validAssigned,
  validUserAssigned,
  putAssigned
} = require('./../app/controllers/vehicles');

/* GET home page. */
router
    .post('/', validData, saveVehicle)    
    .get('/all', getVehicles)
    .get('/:vehicle', getVehicle)
    .put('/:vehicle', validData, putVehicle)
    .put('/assigned/:vehicle', validAssigned, validUserAssigned, putAssigned)
    .delete('/:vehicle', deleteVehicle)


module.exports = router;
