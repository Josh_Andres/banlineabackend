'use strict';
const express = require('express');
const router = express.Router();
const {validData, getAllShipping, saveShipping, } = require('../app/controllers/shippings');

/* shipping routes */
router
			// .get('/:shipping')
      .get('/all', getAllShipping)
      .post('/',validData, saveShipping)
      .put('/:ship')
      .delete('/:ship')

module.exports = router;
