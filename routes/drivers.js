'use strict';
const express = require('express');
const router = express.Router();
const {validData, saveDriver, getDriver, putDriver, deleteDriver, getDrivers} = require('../app/controllers/drivers');

/* routes for drivers */
router
      .post('/',validData, saveDriver)
      .get('/all', getDrivers)
      .get('/:driver', getDriver)
      .put('/:driver', putDriver)
      .delete('/:driver', deleteDriver)

module.exports = router;
